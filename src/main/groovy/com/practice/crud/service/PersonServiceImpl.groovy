package com.practice.crud.service

import com.practice.crud.co.PersonCO
import com.practice.crud.co.SignUpCO
import com.practice.crud.constant.PersonConstant
import com.practice.crud.dto.ResponseDTO
import com.practice.crud.entity.Person
import com.practice.crud.exception.InvalidResponseException
import com.practice.crud.repository.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository personRepository

    @Autowired
    UserServiceImpl userService

    @Override
    ResponseDTO<Person> saveInformation(SignUpCO signUpCO) throws InvalidResponseException {
        ResponseDTO<Person> responseDTO = new ResponseDTO<>()
        try {
            ResponseDTO<Person> response = userService.sinUp(signUpCO)
            if (response.status) {
                responseDTO.setSuccessResponse(response.data, PersonConstant.ADD_SUCCESS)
            } else {
                responseDTO.setFailureResponse(PersonConstant.ERROR_MESSAGE)
            }
        } catch (Exception e) {
            responseDTO.setFailureResponse(PersonConstant.ERROR_MESSAGE)
            throw new InvalidResponseException(e.getMessage())
        }
        return responseDTO
    }

    @Override
    ResponseDTO<List<Person>> getAllRecord() throws InvalidResponseException {
        ResponseDTO<List<Person>> responseDTO = new ResponseDTO()
        try {
            List<Person> personList = personRepository.findAll()
            if (personList) {
                responseDTO.setSuccessResponse(personList, '')
            } else {
                responseDTO.setFailureResponse(PersonConstant.NO_RECORD_EXIST)
            }
        } catch (Exception e) {
            responseDTO.setFailureResponse(null)
            throw new InvalidResponseException(e.getMessage())
        }
        return responseDTO
    }

    @Override
    ResponseDTO<Person> findPersonById(String id) throws InvalidResponseException {
        ResponseDTO<Person> responseDTO = new ResponseDTO()
        try {
            Optional<Person> optionalPerson = personRepository.findById(id)
            if (optionalPerson.isPresent()) {
                Person person = optionalPerson.get()
                responseDTO.setSuccessResponse(person, "")
            } else {
                responseDTO.setFailureResponse(PersonConstant.NO_RECORD_EXIST)
            }
        } catch (Exception e) {
            responseDTO.setFailureResponse(null)
            throw new InvalidResponseException(e.getMessage())
        }
        return responseDTO
    }

    @Override
    ResponseDTO<Person> updateInformation(PersonCO personCO) throws InvalidResponseException {
        ResponseDTO<Person> responseDTO = new ResponseDTO<>()
        try {
            Optional<Person> optionalPerson = personRepository.findById(personCO.id)
            if (optionalPerson.isPresent()) {
                Person person = optionalPerson.get()
                person.name = personCO.name
                person.age = personCO.age
                person.contact = personCO.contact
                person.address = personCO.address
                person.email = personCO.email
                Person updatePerson = personRepository.save(person)
                responseDTO.setSuccessResponse(updatePerson, PersonConstant.UPDATE_SUCCESS)
            } else {
                responseDTO.setFailureResponse(PersonConstant.PERSON_NOT_EXIST)
            }
        } catch (Exception e) {
            responseDTO.setFailureResponse(null)
            throw new InvalidResponseException(e.getMessage())
        }
        return responseDTO
    }

    @Override
    ResponseDTO<Person> deletePersonRecordById(String id) throws InvalidResponseException {
        ResponseDTO<Person> responseDTO = new ResponseDTO()
        try {
            Optional<Person> person = personRepository.findById(id)
            if (person.isPresent()) {
                responseDTO.setSuccessResponse(personRepository.deleteById(id), PersonConstant.DATA_DELETE)
            } else {
                responseDTO.setFailureResponse(PersonConstant.DATA_CAN_NOT_DELETE)
            }
        } catch (Exception e) {
            responseDTO.setFailureResponse(null)
            throw new InvalidResponseException(e.getMessage())
        }
        return responseDTO
    }

}
