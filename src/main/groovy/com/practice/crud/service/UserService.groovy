package com.practice.crud.service

import com.practice.crud.co.SignUpCO
import com.practice.crud.co.UserCO
import com.practice.crud.dto.ResponseDTO
import com.practice.crud.entity.Person
import com.practice.crud.entity.User

interface UserService {

    ResponseDTO<User> login(UserCO userCO)

    ResponseDTO<Person> sinUp(SignUpCO signUpCO)

}