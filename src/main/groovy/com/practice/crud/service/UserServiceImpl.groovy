package com.practice.crud.service

import com.practice.crud.co.SignUpCO
import com.practice.crud.co.UserCO
import com.practice.crud.constant.PersonConstant
import com.practice.crud.dto.ResponseDTO
import com.practice.crud.entity.Person
import com.practice.crud.entity.User
import com.practice.crud.exception.InvalidResponseException
import com.practice.crud.repository.PersonRepository
import com.practice.crud.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository

    @Autowired
    PersonRepository personRepository

    @Override
    ResponseDTO<Person> login(UserCO userCO) throws InvalidResponseException {
        ResponseDTO<Person> responseDTO = new ResponseDTO<>()
        try {
            Optional<User> optionalUser = userRepository.findByPasswordAndEmail(userCO.password, userCO.email)
            Optional<Person> optional = personRepository.findByEmail(userCO.email)
            if (optionalUser.isPresent()) {
                responseDTO.setSuccessResponse(optional.get(), "")
            } else {
                responseDTO.setFailureResponse(null)
            }
        } catch (Exception e) {
            responseDTO.setErrorResponse(e, '')
        }
        return responseDTO
    }

    @Override
    ResponseDTO<Person> sinUp(SignUpCO signUpCO) {
        ResponseDTO<Person> responseDTO = new ResponseDTO<>()
        try {
            Optional<Person> optional = personRepository.findByEmail(signUpCO.email)
            if (optional.isPresent()) {
                responseDTO.setFailureResponse(PersonConstant.EMAIL_EXIST)
            } else {
                User user = new User()
                user.email = signUpCO.email
                user.password = signUpCO.password
                userRepository.save(user)
                Person person = new Person()
                person.name = signUpCO.name
                person.age = signUpCO.age
                person.contact = signUpCO.contact
                person.address = signUpCO.address
                person.email = signUpCO.email
                personRepository.save(person)
                responseDTO.setSuccessResponse(person, PersonConstant.CREATE_SUCCESS)
            }
        } catch (Exception e) {
            responseDTO.setErrorResponse(e, '')
        }
        return responseDTO
    }
}
