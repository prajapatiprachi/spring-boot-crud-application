package com.practice.crud.service

import com.practice.crud.co.PersonCO
import com.practice.crud.co.SignUpCO
import com.practice.crud.dto.ResponseDTO
import com.practice.crud.entity.Person

interface PersonService {

    ResponseDTO<Person> saveInformation(SignUpCO signUpCO)

    ResponseDTO<List<Person>> getAllRecord()

    ResponseDTO<Person> findPersonById(String id)

    ResponseDTO<Person> updateInformation(PersonCO personCO)

    ResponseDTO<Person> deletePersonRecordById(String id)

}