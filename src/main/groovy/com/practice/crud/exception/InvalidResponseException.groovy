package com.practice.crud.exception

class InvalidResponseException extends Exception{

    InvalidResponseException(String message) {
        super(message)
    }
}
