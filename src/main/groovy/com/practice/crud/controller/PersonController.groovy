package com.practice.crud.controller

import com.practice.crud.co.PersonCO
import com.practice.crud.co.SignUpCO
import com.practice.crud.dto.ResponseDTO
import com.practice.crud.entity.Person
import com.practice.crud.service.PersonServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("/person")
@CrossOrigin(value = "http://localhost:4200")
class PersonController {

    @Autowired
    PersonServiceImpl personService

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseDTO<Person> saveInformation(@RequestBody SignUpCO signUpCO) {
        ResponseDTO<Person> responseData = personService.saveInformation(signUpCO)
        return responseData
    }

    @GetMapping(value = "/list")
    ResponseDTO<List<Person>> fetchListOfPerson() {
        ResponseDTO<List<Person>> responseDTO = personService.getAllRecord()
        return responseDTO
    }

    @GetMapping(value = '/findById/{id}')
    ResponseDTO<Person> findPersonById(@PathVariable("id") String id) {
        ResponseDTO<Person> responseDTO = personService.findPersonById(id)
        return responseDTO
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseDTO<Person> updatePerson(@RequestBody PersonCO personCO) {
        ResponseDTO<Person> responseDTO = personService.updateInformation(personCO)
        return responseDTO
    }

    @DeleteMapping(value = "/deleteById/{id}")
    ResponseDTO<Person> deletePerson(@PathVariable("id") String id) {
        ResponseDTO<Person> responseDTO = personService.deletePersonRecordById(id)
        return responseDTO
    }
}
