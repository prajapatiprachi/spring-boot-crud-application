package com.practice.crud.controller

import com.practice.crud.co.SignUpCO
import com.practice.crud.co.UserCO
import com.practice.crud.dto.ResponseDTO
import com.practice.crud.entity.Person
import com.practice.crud.entity.User
import com.practice.crud.service.UserServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("/login")
@CrossOrigin(value = "http://localhost:4200")
class LoginController {

    @Autowired
    UserServiceImpl userService

    @PostMapping(value = "/sinUp", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseDTO<Person> sinUp(@RequestBody SignUpCO signUpCO) {
        ResponseDTO<Person> responseDTO = userService.sinUp(signUpCO)
        return responseDTO
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseDTO<Person> login(@RequestBody UserCO userCO) {
        ResponseDTO<Person> responseDTO = userService.login(userCO)
        return responseDTO
    }
}
