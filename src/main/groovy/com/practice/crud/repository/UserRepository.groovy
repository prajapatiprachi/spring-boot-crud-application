package com.practice.crud.repository

import com.practice.crud.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByPasswordAndEmail(String password, String email)

}