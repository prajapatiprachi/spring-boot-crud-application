package com.practice.crud.constant

class PersonConstant {

    public static final CREATE_SUCCESS = "User create successfully."
    public static final ADD_SUCCESS = "Add user successfully."
    public static final ERROR_MESSAGE = "The operation did not complete successfully."
    public static final UPDATE_SUCCESS = 'Data update successfully.'
    public static final NO_RECORD_EXIST = 'No record exist.'
    public static final DATA_DELETE = "Data delete successfully."
    public static final DATA_CAN_NOT_DELETE = "Data can not be delete because this person is not exist."
    public static final PERSON_NOT_EXIST = 'Person not exist.'
    public static final EMAIL_EXIST = 'This email exist with another user.'
}
