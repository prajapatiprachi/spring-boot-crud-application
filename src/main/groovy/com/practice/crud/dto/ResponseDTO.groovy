package com.practice.crud.dto

class ResponseDTO<T> {

    boolean status = true
    String msg = ''
    T data = data ?: null

    void setSuccessResponse(T data, String msg) {
        this.data = data
        this.msg = msg
    }

    void setFailureResponse(String msg) {
        this.data = null
        this.status = false
        this.msg = msg
    }

    void setErrorResponse(Exception e, String msg) {
        this.msg = msg ?: e.message
        this.data = null
        this.status = false
        e.printStackTrace()
    }
}
